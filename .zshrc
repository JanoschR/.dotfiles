# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/janosch/.zshrc'

autoload -Uz compinit 
compinit

# git features, pretty slow
source ~/.zshres/zsh-git/zsh-git-prompt/zshrc.sh
# an example prompt
autoload -Uz colors && colors
PROMPT=' %~ %b$(git_super_status) '
RPROMPT=' [%n@%m] '
